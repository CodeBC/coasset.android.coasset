package co.nexlabs.android.coassets.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.adapter.ProjectAdapter;
import co.nexlabs.android.coassets.model.Project;
import co.nexlabs.android.coassets.ui.TokenListActivity;
import co.nexlabs.android.coassets.utils.Pref;

/**
 * Created by SH on 02/Jun/2014.
 */
public class ProjectListFragment extends ListFragment {
  /**
   * The fragment argument representing the section number for this
   * fragment.
   */
  private static final String ARG_SECTION_NUMBER = "section_number";
  public static final String TOKEN = "token";
  public static final String TYPE = "acc_type";

  private ArrayList<Project> mItems = new ArrayList<Project>();
  private ProjectAdapter mAdapter;
  private ProgressDialog dialog;
  private Callbacks mCallbacks;
  @InjectView(R.id.project_list_view)
  ListView listView;
  LinearLayout mHeaderView;
  private Pref pref;
  int section;
  String accType;
  String token;

  public static ProjectListFragment newInstance(int sectionNumber, String acc, String token) {
    ProjectListFragment fragment = new ProjectListFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    args.putString(TYPE, acc);
    args.putString(TOKEN, token);
    fragment.setArguments(args);
    return fragment;
  }

  public ProjectListFragment() {
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    try {
      mCallbacks = (Callbacks) activity;
    } catch (ClassCastException ex) {
      Log.e("", "Casting the activity as a Callbacks listener failed: " + ex);
      mCallbacks = null;
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_projects, container, false);
    dialog = new ProgressDialog(getActivity());
    ButterKnife.inject(this, rootView);
    section = getArguments().getInt(ARG_SECTION_NUMBER);
    accType = getArguments().getString(TYPE);
    token = getArguments().getString(TOKEN);
    Log.e("", "Acc: " + accType + " || " + "Token: " + token);
    pref = Pref.getInstance(getActivity());

    return rootView;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    LayoutInflater inflater = getActivity().getLayoutInflater();
    mHeaderView = (LinearLayout) inflater.inflate(R.layout.list_header, listView, false);

    TextView tvAcc = (TextView) mHeaderView.findViewById(R.id.account_type);
    TextView tvToken = (TextView) mHeaderView.findViewById(R.id.token);

    if (accType == null || token == null) {
      tvAcc.setText(pref.getAccType());
      tvToken.setText(pref.getToken());
    } else {
      tvAcc.setText(accType);
      tvToken.setText(token);
    }

    ImageView buy = (ImageView) mHeaderView.findViewById(R.id.btn_buy_token);
    buy.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent();
        i.setClass(getActivity().getApplicationContext(), TokenListActivity.class);
        startActivity(i);
      }
    });

    listView.addHeaderView(mHeaderView, null, false);
    getAllProjects();
  }

  private void getAllProjects() {
    dialog.setMessage("Loading Projects");
    dialog.show();

    String url = "";

    if (section == 1) {
      url = "https://coassets.com/api/profile/projects/";
    } else if (section == 2) {
      url = "https://coassets.com/api/profile/own_projects/";
    }

    Ion.with(getActivity())
        .load(url)
        .setLogging("iON", Log.DEBUG)
        .setHeader("Authorization", "Bearer " + pref.getAccessToken())
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {
            Log.e("", "Result: " + result);
            dialog.dismiss();
            if (result == null) {
              Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
            } else if (result.equalsIgnoreCase("[]")) {
              Toast.makeText(getActivity(), "Nothing yet.", Toast.LENGTH_LONG).show();
            } else {
              JsonParser parser = new JsonParser();
              JsonArray jArray = (JsonArray) parser.parse(result);
              Gson gson = new GsonBuilder().create();

              for (int i = 0; i < jArray.size(); i++) {
                JsonObject obj = jArray.get(i).getAsJsonObject();
                Project item = gson.fromJson(obj, Project.class);
                mItems.add(item);
              }

              mAdapter = new ProjectAdapter(getActivity(), mItems);
              listView.setAdapter(mAdapter);
            }
          }
        });
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    super.onListItemClick(l, v, position, id);
    if (mCallbacks != null) {
      mCallbacks.onItemSelected(mItems.get(position - 1)); // When the header is added, the list item is started counting from 1.
    }
  }

  public interface Callbacks {
    public void onItemSelected(Project project);
  }
}
