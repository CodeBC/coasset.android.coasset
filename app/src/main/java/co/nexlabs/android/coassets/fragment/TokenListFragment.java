package co.nexlabs.android.coassets.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.adapter.TokenAdapter;
import co.nexlabs.android.coassets.base.BaseFragment;
import co.nexlabs.android.coassets.model.Token;
import co.nexlabs.android.coassets.utils.Pref;

/**
 * Created by SH on 05/Jun/2014.
 */
public class TokenListFragment extends BaseFragment implements AdapterView.OnItemClickListener {
  private ArrayList<Token> mItems = new ArrayList<Token>();
  private TokenAdapter mAdapter;
  private ProgressDialog dialog;
  private Callbacks mCallbacks;
  @InjectView(R.id.token_list)
  ListView listView;
  private Pref pref;

  public static TokenListFragment newInstance() {
    TokenListFragment fragment = new TokenListFragment();
    return fragment;
  }

  public TokenListFragment() {
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    try {
      mCallbacks = (Callbacks) activity;
    } catch (ClassCastException ex) {
      Log.e("", "Casting the activity as a Callbacks listener failed: " + ex);
      mCallbacks = null;
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_token_view, container, false);
    dialog = new ProgressDialog(getActivity());
    ButterKnife.inject(this, rootView);
    pref = Pref.getInstance(getActivity());

    getAllTokens();

    listView.setOnItemClickListener(this);

    return rootView;
  }

  private void getAllTokens() {
    dialog.setMessage("Loading Tokens");
    dialog.show();

    String url = "https://coassets.com/api/product/tokens/";

    Ion.with(getActivity())
        .load(url)
        .setLogging("iON", Log.DEBUG)
        .setHeader("Authorization", "Bearer " + pref.getAccessToken())
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {
            Log.e("", "Result: " + result);
            dialog.dismiss();
            if (result == null) {
              Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
            } else if (result.equalsIgnoreCase("[]")) {
              Toast.makeText(getActivity(), "Nothing yet.", Toast.LENGTH_LONG).show();
            } else {
              JsonParser parser = new JsonParser();
              JsonArray jArray = (JsonArray) parser.parse(result);
              Gson gson = new GsonBuilder().create();

              for (int i = 0; i < jArray.size(); i++) {
                JsonObject obj = jArray.get(i).getAsJsonObject();
                Token item = gson.fromJson(obj, Token.class);
                mItems.add(item);
              }

              mAdapter = new TokenAdapter(getActivity(), mItems);
              listView.setAdapter(mAdapter);
            }
          }
        });
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    if (mCallbacks != null) {
      mCallbacks.onItemSelected(mItems.get(position));
    }
  }

  public interface Callbacks {
    public void onItemSelected(Token project);
  }
}
