package co.nexlabs.android.coassets.utils;

public class Constants {
    public static final String DEFAULT_SHARE_DATA = "coassets";
    public static final String BASE_URL = "https://coassets.com";
    public static final String TELLME_MORE = "/api/offers/tellmemore/";
    public static final String SUBSCRIBE_API = "/api/offers/subscribe/";
    public static final String PROFILE_API = "/api/profile/";
    public static final String PASSWORD_API = "/api/change-password/";
    public static final String REGISTER_API = "/api/profile/register/";
}
