package co.nexlabs.android.coassets.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.api.ProfileAPI;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.model.UserItem;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegisterActivity extends BaseActivity {
    String usrName = "";
    String fName = "";
    String lName = "";
    String ph = "";
    String mEmail = "";
    String mEmailConf = "";
    String pswd = "";
    String ctry = "";

    @InjectView(R.id.user_name)
    EditText username;
    @InjectView(R.id.first_name)
    EditText firstName;
    @InjectView(R.id.last_name)
    EditText lastName;
    @InjectView(R.id.ph_num)
    EditText phNum;
    @InjectView(R.id.email)
    EditText email;
    @InjectView(R.id.email_conf)
    EditText emailConf;
    @InjectView(R.id.password)
    EditText password;
    @InjectView(R.id.country)
    EditText country;

    UserItem mUser = new UserItem();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.inject(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        dialog = new ProgressDialog(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_sign_up)
    public void doSignUp() {
        boolean pass = validForms();
        if (pass) {
            dialog.setMessage("Loading");
            dialog.show();
            Log.d("", "" + usrName + ", " + fName + ", " + lName + ", " + ph + ", " + mEmail + ", " + pswd);

            ProfileAPI.getInstance(context).getService().registerUser(usrName, mEmail, pswd, lName, fName, ctry, ph, new Callback<String>() {
                @Override
                public void success(String userItem, Response response) {
                    dialog.dismiss();
                    if (response.getStatus() == 200) {
                        Toast.makeText(context, "Account Created Successfully.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dialog.dismiss();
                    Log.d("", "Error is: " + error.getResponse().getStatus());
                    if (error.getResponse().getStatus() == 400) {
                        Toast.makeText(context, "User Name is already exist.", Toast.LENGTH_SHORT).show();
                        username.requestFocus();
                    }
                    if (error.getResponse().getStatus() == 200) {
                        Toast.makeText(context, "Account Created Successfully.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        }
    }

    public boolean validForms() {
        usrName = username.getText().toString();
        fName = firstName.getText().toString();
        lName = lastName.getText().toString();
        ph = phNum.getText().toString();
        mEmail = email.getText().toString();
        mEmailConf = emailConf.getText().toString();
        ctry = country.getText().toString();
        pswd = password.getText().toString();

        if (usrName.equalsIgnoreCase("") || fName.equalsIgnoreCase("") || lName.equalsIgnoreCase("") || ph.equalsIgnoreCase("") || mEmail.equalsIgnoreCase("") || mEmailConf.equalsIgnoreCase("") || pswd.equalsIgnoreCase("") || ctry.equalsIgnoreCase("")) {
            Toast.makeText(context, "Please fill all the form.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!mEmail.equalsIgnoreCase(mEmailConf)) {
            Toast.makeText(context, "Email address didn't match.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
