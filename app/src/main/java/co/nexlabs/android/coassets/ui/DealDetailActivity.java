package co.nexlabs.android.coassets.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.api.TellMeMoreAPI;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.model.OnGoingItem;
import co.nexlabs.android.coassets.model.Project;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by SH on 16/May/2014.
 */
public class DealDetailActivity extends BaseActivity {

  public static final String ARG_DEAL_ID = "deal_id";
  public static final String ARG_DEAL_TYPE = "deal_type";
  private OnGoingItem onGoingItem;
  private Project projectItem;
  private String id;
  @InjectView(R.id.deal_title)
  TextView title;
  @InjectView(R.id.deal_type)
  TextView dealType;
  @InjectView(R.id.ongoing_img_IV)
  ImageView image;
  @InjectView(R.id.interested_text)
  TextView interested;
  @InjectView(R.id.proj_type)
  TextView projectType;
  @InjectView(R.id.fancy)
  TextView fancy;
  @InjectView(R.id.address)
  TextView address;
  @InjectView(R.id.currency)
  TextView currency;
  @InjectView(R.id.min_amount)
  TextView minAmount;
  @InjectView(R.id.horizon)
  TextView horizon;
  @InjectView(R.id.annual_return)
  TextView annualReturn;
  @InjectView(R.id.description)
  TextView desc;
  @InjectView(R.id.linearlayout)
  LinearLayout linearLayout;
  @InjectView(R.id.tell_me_more)
  Button tellMeMore;
  @InjectView(R.id.subscribe)
  Button subscribe;
  private ProgressDialog dialog;
  OnGoingItem item;
  int type;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_deal_detail);
    ButterKnife.inject(this);
    dialog = new ProgressDialog(this);

    type = getIntent().getIntExtra(ARG_DEAL_TYPE, 1);

    if (type == 1) {
      onGoingItem = (OnGoingItem) getIntent().getExtras().getSerializable(ARG_DEAL_ID);
      getDealDetail(onGoingItem.getId());
    } else if (type == 2) {
      projectItem = (Project) getIntent().getExtras().getSerializable(ARG_DEAL_ID);
      getDealDetail(projectItem.getProject_id());
    }

    getActionBar().setDisplayHomeAsUpEnabled(true);
  }

  private void getDealDetail(final int id) {
    dialog.setMessage("Loading Deal");
    dialog.show();
    Ion.with(this)
        .load("https://coassets.com/api/offers/" + id)
        .setLogging("iON", Log.DEBUG)
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {
            dialog.dismiss();
            Log.e("ION", "Deal Detail Result : " + result);

            if (result == null) {
              Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_LONG).show();
            } else {
              JsonParser parser = new JsonParser();
              JsonObject jObj = (JsonObject) parser.parse(result);
              Gson gson = new GsonBuilder().create();

              item = gson.fromJson(jObj, OnGoingItem.class);

              linearLayout.setVisibility(View.VISIBLE);
              title.setText(item.getOffer_title());
              dealType.setText(item.getOffer_type());
              interested.setText("Interested Investor: " + item.getInterested());
              //daysLeft.setText("0 day left");
              projectType.setText("Project Type: " + item.getProject().getProject_type());
              fancy.setText("Fancy (Participate) : " + item.getTokens_per_fancy() + " tokens");
              address.setText("Address: " + item.getProject().getAddress_1());
              currency.setText("Currency: " + item.getCurrency());
              minAmount.setText("Min. Amt: " + item.getCurrency() + " " + NumberFormat.getNumberInstance(Locale.US).format(item.getMin_investment()));
              horizon.setText("Horizon (mths): " + item.getTime_horizon());
              if (item.getMin_annual_return() == null) {
                annualReturn.setText("Annualized Return : Not Available");
              } else {
                annualReturn.setText("Annualized Return : " + item.getMin_annual_return() + " %");
              }

              desc.setText(Html.fromHtml(item.getDescription()));
              getImage();
            }
          }
        });
  }

  public void getImage() {
    Ion.with(image)
        .smartSize(true)
        .load(item.getProject().getPhoto());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.tell_me_more)
  public void tellMeMore() {
    dialog.setMessage("Sending Email");
    dialog.show();
    Log.e("", "AUTH : " + pref.getAccessToken());
    int id = 0;
    if (type == 1) {
      id = onGoingItem.getId();
    } else if (type == 2) {
      id = projectItem.getProject_id();
    }
    TellMeMoreAPI.getInstance(context).getService().tellMeMore("Bearer " + pref.getAccessToken(), id, new Callback<JsonObject>() {
      @Override
      public void success(JsonObject s, retrofit.client.Response response) {
        dialog.dismiss();
        Log.e("", "Response: " + response.getStatus());
        Toast.makeText(context, "Email Sent", Toast.LENGTH_SHORT).show();
        tellMeMore.setEnabled(false);
      }

      @Override
      public void failure(RetrofitError error) {
        dialog.dismiss();
        Log.e("", "Response: " + error.getLocalizedMessage());
        Toast.makeText(context, "Email Sent", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @OnClick(R.id.subscribe)
  public void subscribe() {
    dialog.setMessage("Please wait...");
    dialog.show();
    int id = 0;
    if (type == 1) {
      id = onGoingItem.getId();
    } else if (type == 2) {
      id = projectItem.getProject_id();
    }
    TellMeMoreAPI.getInstance(context).getService().subscribe("Bearer " + pref.getAccessToken(), id, new Callback<JsonObject>() {
      @Override
      public void success(JsonObject s, retrofit.client.Response response) {
        dialog.dismiss();
        Log.e("", "Response: " + response.getStatus());
        Toast.makeText(context, "Subscribed", Toast.LENGTH_SHORT).show();
        subscribe.setEnabled(false);
      }

      @Override
      public void failure(RetrofitError error) {
        dialog.dismiss();
        Log.e("", "Response: " + error.getLocalizedMessage());
      }
    });
  }
}
