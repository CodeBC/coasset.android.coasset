package co.nexlabs.android.coassets.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseFragment;
import co.nexlabs.android.coassets.model.Token;

/**
 * Created by SH on 20/Jun/2014.
 */
public class TokenBuyFragment extends BaseFragment {
  @InjectView(R.id.web_page)
  WebView webView;

  public static final String ARG_TOKEN_ID = "token_id";
  private Token mToken;
  private String mData;
  private ProgressDialog dialog;

  public static TokenBuyFragment newInstance(Token tokenId) {
    TokenBuyFragment fragment = new TokenBuyFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_TOKEN_ID, tokenId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_token_buy, container, false);
    ButterKnife.inject(this, view);
    dialog = new ProgressDialog(getActivity());

    mToken = (Token) getArguments().getSerializable(ARG_TOKEN_ID);
    getHTML(mToken.getProduct_code());

    return view;
  }

  public void getHTML(String tokenId) {
    dialog.setMessage("Loading Tokens");
    dialog.show();

    String url = "https://coassets.com/api/paypal/payment/";

    Ion.with(getActivity())
        .load(url)
        .setLogging("iON", Log.DEBUG)
        .setHeader("Authorization", "Bearer " + pref.getAccessToken())
        .setBodyParameter("product_code", tokenId)
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {
            Log.e("", "Result: " + result);
            dialog.dismiss();
            if (result == null) {
              Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
            } else if (result.equalsIgnoreCase("[]")) {
              Toast.makeText(getActivity(), "Nothing yet.", Toast.LENGTH_LONG).show();
            } else {
              JsonParser parser = new JsonParser();
              JsonObject jsonObject = (JsonObject) parser.parse(result);

              mData = jsonObject.get("html").getAsString();

              if (!mData.equalsIgnoreCase("")) {
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebViewClient(new webClient());
                webView.loadData(mData, "text/html", null);
              }
            }
          }
        });
  }

  private class webClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      return false;
    }
  }
}
