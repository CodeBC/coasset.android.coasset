package co.nexlabs.android.coassets.api;

import android.content.Context;

import co.nexlabs.android.coassets.utils.Constants;
import retrofit.RestAdapter;

/**
 * Created by SH on 23/May/2014.
 */
public class ProfileAPI {
    private static ProfileAPI mInstance;
    private Context mContext;
    private ProfileService mService;

    public ProfileAPI(Context context) {
        this.mContext = context;

        final RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(Constants.BASE_URL).build();
        mService = restAdapter.create(ProfileService.class);
    }

    public static ProfileAPI getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ProfileAPI(context);
        }
        return mInstance;
    }

    public ProfileService getService() {
        return mService;
    }
}
