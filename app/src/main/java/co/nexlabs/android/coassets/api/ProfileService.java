package co.nexlabs.android.coassets.api;

import co.nexlabs.android.coassets.model.UserItem;
import co.nexlabs.android.coassets.utils.Constants;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;

/**
 * Created by SH on 23/May/2014.
 */
public interface ProfileService {
  @GET(Constants.PROFILE_API)
  void getUserInfo(@Header("Authorization") String authorization, Callback<UserItem> callback);

  @FormUrlEncoded
  @POST(Constants.PASSWORD_API)
  void changePassword(@Header("Authorization") String authorization, @Field("new_password") String newPassword, Callback<String> callback);

  @FormUrlEncoded
  @PUT(Constants.PROFILE_API)
  void updateProfile(@Header("Authorization") String authorization, @Field("username") String username, @Field("email") String email, @Field("last_name") String lastName, @Field("first_name") String firstName, @Field("country_prefix") String country_prefix, @Field("cell_phone") String cellPhone, @Field("address") String address, @Field("city") String city, @Field("country") String country, Callback<UserItem> callback);

  @FormUrlEncoded
  @POST(Constants.REGISTER_API)
  void registerUser(@Field("username") String username, @Field("email") String email, @Field("password") String password, @Field("last_name") String lastName, @Field("first_name") String firstName, @Field("country_prefix") String country, @Field("cell_phone") String cellPhone, Callback<String> callback);
}
