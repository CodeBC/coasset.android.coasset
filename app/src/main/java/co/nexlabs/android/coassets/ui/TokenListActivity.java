package co.nexlabs.android.coassets.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.fragment.TokenBuyFragment;
import co.nexlabs.android.coassets.fragment.TokenListFragment;
import co.nexlabs.android.coassets.model.Token;

/**
 * Created by SH on 05/Jun/2014.
 */
public class TokenListActivity extends BaseActivity implements TokenListFragment.Callbacks {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_token_purchase);

    getFragmentManager().beginTransaction()
        .replace(R.id.container, TokenListFragment.newInstance())
        .commit();

    getActionBar().setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onItemSelected(Token obj) {
    Toast.makeText(context, obj.getProduct_code(), Toast.LENGTH_SHORT).show();

    getFragmentManager().beginTransaction()
        .replace(R.id.container, TokenBuyFragment.newInstance(obj))
        .commit();
  }
}
