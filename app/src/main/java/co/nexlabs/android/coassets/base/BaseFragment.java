package co.nexlabs.android.coassets.base;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import co.nexlabs.android.coassets.utils.Pref;

/**
 * Created by SH on 23/May/2014.
 */
public class BaseFragment extends Fragment {
    protected Pref pref;
    protected Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Pref.getInstance(getActivity());
        context = this.getActivity().getApplicationContext();
        Crashlytics.start(context);
    }
}
