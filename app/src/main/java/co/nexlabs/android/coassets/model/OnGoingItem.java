package co.nexlabs.android.coassets.model;

import java.io.Serializable;

/**
 * Created by SH on 16/May/2014.
 */
public class OnGoingItem implements Serializable {
    int id;
    String owner_type;
    String offer_type;
    String offer_title;
    String min_annual_return;
    String max_annual_return;
    String time_horizon;
    int min_investment;
    String currency;
    String short_description;
    int interested;
    ProjectItem project;
    String highlight;
    String description;
    int tokens_per_fancy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwner_type() {
        return owner_type;
    }

    public void setOwner_type(String owner_type) {
        this.owner_type = owner_type;
    }

    public String getOffer_type() {
        if (offer_type.equalsIgnoreCase("CO")) {
            return "Crowd Funding";
        } else if (offer_type.equalsIgnoreCase("BP")) {
            return "Bulk Purchase";
        }
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public String getOffer_title() {
        return offer_title;
    }

    public void setOffer_title(String offer_title) {
        this.offer_title = offer_title;
    }

    public String getMin_annual_return() {
        return min_annual_return;
    }

    public void setMin_annual_return(String min_annual_return) {
        this.min_annual_return = min_annual_return;
    }

    public String getMax_annual_return() {
        return max_annual_return;
    }

    public void setMax_annual_return(String max_annual_return) {
        this.max_annual_return = max_annual_return;
    }

    public String getTime_horizon() {
      if (time_horizon == null) {
        return "0";
      } else {
        return time_horizon;
      }
    }

    public void setTime_horizon(String time_horizon) {
        this.time_horizon = time_horizon;
    }

    public int getMin_investment() {
        return min_investment;
    }

    public void setMin_investment(int min_investment) {
        this.min_investment = min_investment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public int getInterested() {
        return interested;
    }

    public void setInterested(int interested) {
        this.interested = interested;
    }

    public ProjectItem getProject() {
        return project;
    }

    public void setProject(ProjectItem project) {
        this.project = project;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTokens_per_fancy() {
        return tokens_per_fancy;
    }

    public void setTokens_per_fancy(int tokens_per_fancy) {
        this.tokens_per_fancy = tokens_per_fancy;
    }
}
