package co.nexlabs.android.coassets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SH on 22/May/2014.
 */
public class ProfileItem {
    int id;
    String country_prefix;
    int cell_phone;
    String address_1;
    String address_2;
    String city;
    String region_state;
    String country;

    public ProfileItem() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry_prefix() {
        return country_prefix;
    }

    public void setCountry_prefix(String country_prefix) {
        this.country_prefix = country_prefix;
    }

    public int getCell_phone() {
        return cell_phone;
    }

    public void setCell_phone(int cell_phone) {
        this.cell_phone = cell_phone;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion_state() {
        return region_state;
    }

    public void setRegion_state(String region_state) {
        this.region_state = region_state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
