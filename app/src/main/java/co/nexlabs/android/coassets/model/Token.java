package co.nexlabs.android.coassets.model;

import java.io.Serializable;

/**
 * Created by SH on 05/Jun/2014.
 */
public class Token implements Serializable {
  String product_code;
  String description;
  String price;
  String currency;

  public String getProduct_code() {
    return product_code;
  }

  public void setProduct_code(String product_code) {
    this.product_code = product_code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}
