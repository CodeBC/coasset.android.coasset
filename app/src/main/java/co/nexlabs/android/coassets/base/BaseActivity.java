package co.nexlabs.android.coassets.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import co.nexlabs.android.coassets.utils.Pref;

/**
 * Created by SH on 5/2/14.
 */
public class BaseActivity extends Activity {
    protected Pref pref;
    protected Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Pref.getInstance(this);
        context = this.getApplicationContext();
        Crashlytics.start(this);
    }
}
