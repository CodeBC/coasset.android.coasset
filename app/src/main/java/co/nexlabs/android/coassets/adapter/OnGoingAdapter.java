package co.nexlabs.android.coassets.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.model.OnGoingItem;

/**
 * Created by SH on 16/May/2014.
 */
public class OnGoingAdapter extends ArrayAdapter<OnGoingItem> {
    private final Context mContext;
    private ArrayList<OnGoingItem> deals = new ArrayList<OnGoingItem>();

    public OnGoingAdapter(Context context, ArrayList<OnGoingItem> deal) {
        super(context, R.layout.ongoing_item, deal);

        this.mContext = context;
        this.deals = deal;
        Log.e("", "News Size (Adapter): " + deals.size());
    }

    private ViewHolder getHolder(View v) {
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.ongoing_item, null);
            holder = getHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        Ion.with(holder.imageView)
                .smartSize(true)
                .placeholder(R.drawable.placeholder)
                .load(deals.get(position).getProject().getPhoto());
        holder.title.setText(deals.get(position).getOffer_title());

        holder.interestedCount.setText("Interested Investor: " + deals.get(position).getInterested());

        holder.min_investment_text.setText("Min Investment: " + deals.get(position).getCurrency() + " " + NumberFormat.getNumberInstance(Locale.US).format(deals.get(position).getMin_investment()));

        if (deals.get(position).getMin_annual_return() == null) {
            holder.annual_return_text.setText("Annualized Return (%) : Not Available");
        } else {
            holder.annual_return_text.setText("Min Annualized Return (%) : " + deals.get(position).getMin_annual_return() + "%");
        }

        return vi;
    }

    static class ViewHolder {
        @InjectView(R.id.ongoing_img_IV)
        ImageView imageView;
        @InjectView(R.id.ongoing_text)
        TextView title;
        @InjectView(R.id.interested_text)
        TextView interestedCount;
        @InjectView(R.id.annual_return_text)
        TextView annual_return_text;
        @InjectView(R.id.min_investment_text)
        TextView min_investment_text;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
