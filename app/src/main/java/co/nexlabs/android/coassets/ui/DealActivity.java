package co.nexlabs.android.coassets.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.fragment.ClosedFragment;
import co.nexlabs.android.coassets.fragment.OnGoingFragment;
import co.nexlabs.android.coassets.model.OnGoingItem;

/**
 * Created by SH on 16/May/2014.
 */
public class DealActivity extends BaseActivity implements OnGoingFragment.Callbacks, ClosedFragment.Callbacks {
    public static final String ARG_SECTION_ID = "section_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing);

        int section = getIntent().getExtras().getInt(ARG_SECTION_ID);

        switch (section) {
            case 1:
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, OnGoingFragment.newInstance())
                        .commit();
                getActionBar().setTitle("Ongoing Deals");
                break;
            case 2:
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, ClosedFragment.newInstance())
                        .commit();
                getActionBar().setTitle("Closed Deals");
                break;
            case 3:
                Intent i = new Intent();
                i.setClass(context, ProjectsActivity.class);
                startActivity(i);
                break;
        }
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(OnGoingItem onGoingItem) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(DealDetailActivity.ARG_DEAL_ID, onGoingItem);
        arguments.putInt(DealDetailActivity.ARG_DEAL_TYPE, 1);
        Intent i = new Intent();
        i.putExtras(arguments);
        i.setClass(this, DealDetailActivity.class);
        startActivity(i);
    }
}
