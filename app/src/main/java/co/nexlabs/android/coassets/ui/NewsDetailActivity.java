package co.nexlabs.android.coassets.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.model.NewsItem;

/**
 * Created by SH on 5/5/14.
 */
public class NewsDetailActivity extends Activity {

  public static final String ARG_NEWS_ID = "news_id";
  private NewsItem newsItem;
  private String id;
  @InjectView(R.id.news_title)
  TextView title;
  @InjectView(R.id.news_date)
  TextView date;
  @InjectView(R.id.image_view)
  ImageView image;
  @InjectView(R.id.news)
  TextView news;
  private ProgressDialog dialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_news_detail);
    ButterKnife.inject(this);
    dialog = new ProgressDialog(this);
    newsItem = (NewsItem) getIntent().getExtras().getSerializable(ARG_NEWS_ID);
    getNewsDetail(newsItem.getId());
    getActionBar().setDisplayHomeAsUpEnabled(true);
  }

  private void getNewsDetail(final String id) {
    dialog.setMessage("Loading News");
    dialog.show();
    Ion.with(this)
        .load("https://coassets.com/api/news/" + id)
        .setLogging("iON", Log.DEBUG)
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {
            dialog.dismiss();

            JsonParser parser = new JsonParser();
            JsonObject jObj = (JsonObject) parser.parse(result);
            Gson gson = new GsonBuilder().create();

            NewsItem item = gson.fromJson(jObj, NewsItem.class);

            title.setText(item.getTitle());
            date.setText(item.getDate());
            news.setText(Html.fromHtml(item.getContent()));
          }
        });
  }
}
