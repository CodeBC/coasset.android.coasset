package co.nexlabs.android.coassets.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.model.Project;

/**
 * Created by SH on 02/Jun/2014.
 */
public class ProjectAdapter extends ArrayAdapter<Project> {
    private final Context mContext;
    private ArrayList<Project> projects = new ArrayList<Project>();

    public ProjectAdapter(Context context, ArrayList<Project> projects) {
        super(context, R.layout.project_item, projects);

        this.mContext = context;
        this.projects = projects;
        Log.e("", "Project Size (Adapter): " + projects.size());
    }

    private ViewHolder getHolder(View v) {
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.project_item, null);
            holder = getHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        Ion.with(holder.imageView)
            .smartSize(true)
            .placeholder(R.drawable.placeholder)
            .load(projects.get(position).getImage_url());
        holder.name.setText(projects.get(position).getProject_name());
        holder.desc.setText(projects.get(position).getShort_description());

        return vi;
    }

    static class ViewHolder {
        @InjectView(R.id.image_view)
        ImageView imageView;
        @InjectView(R.id.txt_prj_name)
        TextView name;
        @InjectView(R.id.txt_prj_desc)
        TextView desc;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
