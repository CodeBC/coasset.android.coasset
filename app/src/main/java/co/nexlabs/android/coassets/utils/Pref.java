package co.nexlabs.android.coassets.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SH on 5/2/14.
 */

public class Pref {
    private static final String Pref_User_Learned_Drawer = "navigation_drawer_learned";
    private static Pref pref;
    public String mAccessToken = "access_token";
    public String mRefreshToken = "refresh_token";
    public String accType = "account_type";
    public String token = "token";
    public String mAccExpirary = "account_expiry";
    private SharedPreferences mPreference;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    public Pref(Context context) {
        this.mContext = context;
        mPreference = context.getSharedPreferences(Constants.DEFAULT_SHARE_DATA, 0);
        mEditor = mPreference.edit();
    }

    public static synchronized Pref getInstance(Context context) {
        if (pref == null) {
            pref = new Pref(context);
        }

        return pref;
    }

    public void clearAll() {
        mEditor.clear();
        mEditor.commit();
    }

    public boolean isExist(String key) {
        return mPreference.contains(Util.SHA1(key));
    }

    public String getAccessToken() {
        return mPreference.getString(mAccessToken, "");
    }

    public void setAccessToken(String accessToken) {
        mEditor.putString(mAccessToken, Util.SHA1(accessToken));
        mEditor.commit();
    }

  public String getAccType() {
    return accType;
  }

  public void setAccType(String accType) {
    this.accType = accType;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public void setExpiry(String date) {
        mEditor.putString(mAccExpirary, date);
        mEditor.commit();
    }

    public String getExpiry() {
        return mPreference.getString(mAccExpirary, "");
    }
}
