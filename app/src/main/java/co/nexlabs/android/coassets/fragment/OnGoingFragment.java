package co.nexlabs.android.coassets.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.adapter.OnGoingAdapter;
import co.nexlabs.android.coassets.model.OnGoingItem;

/**
 * Created by SH on 16/May/2014.
 */
public class OnGoingFragment extends ListFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    //private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<OnGoingItem> mItems = new ArrayList<OnGoingItem>();
    private OnGoingAdapter mAdapter;
    private ProgressDialog dialog;
    private Callbacks mCallbacks;
    @InjectView(R.id.ongoing_deal_list)
    ListView listView;

    public static OnGoingFragment newInstance() {
        OnGoingFragment fragment = new OnGoingFragment();
        return fragment;
    }

    public OnGoingFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallbacks = (Callbacks) activity;
        } catch (ClassCastException ex) {
            Log.e("", "Casting the activity as a Callbacks listener failed: " + ex);
            mCallbacks = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ongoing, container, false);
        dialog = new ProgressDialog(getActivity());
        ButterKnife.inject(this, rootView);

        getAllNews();

        return rootView;
    }

    private void getAllNews() {
        dialog.setMessage("Loading Ongoing Deals");
        dialog.show();
        Ion.with(getActivity())
                .load("https://coassets.com/api/offers/")
                .setLogging("iON", Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.e("", "Result: " + result);
                        dialog.dismiss();
                        if (result == null) {
                            Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
                        } else {
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = (JsonArray) parser.parse(result);
                            Gson gson = new GsonBuilder().create();

                            for (int i = 0; i < jArray.size(); i++) {
                                JsonObject obj = jArray.get(i).getAsJsonObject();
                                OnGoingItem item = gson.fromJson(obj, OnGoingItem.class);
                                mItems.add(item);
                            }

                            mAdapter = new OnGoingAdapter(getActivity(), mItems);
                            listView.setAdapter(mAdapter);
                        }
                    }
                });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (mCallbacks != null) {
            mCallbacks.onItemSelected(mItems.get(position));
        }
    }

    public interface Callbacks {
        public void onItemSelected(OnGoingItem onGoingItem);
    }
}
