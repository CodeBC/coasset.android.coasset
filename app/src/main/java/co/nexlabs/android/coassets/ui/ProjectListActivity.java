package co.nexlabs.android.coassets.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.fragment.ProjectListFragment;
import co.nexlabs.android.coassets.model.Project;

/**
 * Created by SH on 02/Jun/2014.
 */
public class ProjectListActivity extends BaseActivity implements ProjectListFragment.Callbacks {

  public static final String ARG_SECTION_ID = "section_id";
  public static final String TOKEN = "token";
  public static final String TYPE = "acc_type";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_project_list);
    int section = getIntent().getExtras().getInt(ARG_SECTION_ID);
    String acc = getIntent().getExtras().getString(TYPE);
    String token = getIntent().getExtras().getString(TOKEN);

    switch (section) {
      case 1:
        getFragmentManager().beginTransaction()
            .replace(R.id.container, ProjectListFragment.newInstance(section, acc, token))
            .commit();
        getActionBar().setTitle("Participated Projects");
        break;
      case 2:
        getFragmentManager().beginTransaction()
            .replace(R.id.container, ProjectListFragment.newInstance(section, acc, token))
            .commit();
        getActionBar().setTitle("Owned Projects");
        break;
    }

    getActionBar().setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onItemSelected(Project obj) {
    Log.e("", "Selected: " + obj.getProject_name());
    Bundle arguments = new Bundle();
    arguments.putSerializable(DealDetailActivity.ARG_DEAL_ID, obj);
    arguments.putInt(DealDetailActivity.ARG_DEAL_TYPE, 2);
    Intent i = new Intent();
    i.putExtras(arguments);
    i.setClass(this, DealDetailActivity.class);
    startActivity(i);
  }
}
