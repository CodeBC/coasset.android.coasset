package co.nexlabs.android.coassets.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import co.nexlabs.android.coassets.BuildConfig;

/**
 * @author Hein Win Toe
 * @version 1.0
 */

public class Util {

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte)
                        : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) {
        if (BuildConfig.DEBUG) {
            return text;
        } else {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(text.getBytes("iso-8859-1"), 0, text.length());
                byte[] sha1hash = md.digest();
                return convertToHex(sha1hash);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return e.getMessage();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }
    }

    public static String SHA(String text) {
        if (BuildConfig.DEBUG) {
            return text;
        } else {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(text.getBytes("iso-8859-1"), 0, text.length());
                byte[] sha1hash = md.digest();
                return convertToHex(sha1hash);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return e.getMessage();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }
    }
}
