package co.nexlabs.android.coassets.model;

/**
 * Created by SH on 22/May/2014.
 */
public class TokenItem {
    int id;
    int credit_qty;

    public TokenItem() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCredit_qty() {
        return credit_qty;
    }

    public void setCredit_qty(int credit_qty) {
        this.credit_qty = credit_qty;
    }
}
