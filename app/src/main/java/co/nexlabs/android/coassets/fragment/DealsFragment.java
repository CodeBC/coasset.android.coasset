package co.nexlabs.android.coassets.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.ui.DealActivity;
import co.nexlabs.android.coassets.ui.ProjectsActivity;

/**
 * Created by SH on 08/May/2014.
 */
public class DealsFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static DealsFragment newInstance(int sectionNumber) {
        DealsFragment fragment = new DealsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DealsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_deals, container, false);
        ButterKnife.inject(this, rootView);

        return rootView;
    }

    @OnClick({R.id.ongoing_deals_FL, R.id.closed_deals_FL, R.id.my_deals_FL})
    public void dealsOnClick(View view) {
        int id = view.getId();
        Intent i = new Intent();
        Bundle arguments = new Bundle();
        switch (id) {
            case R.id.ongoing_deals_FL:
                arguments.putInt(DealActivity.ARG_SECTION_ID, 1);
                i.putExtras(arguments);
                i.setClass(getActivity(), DealActivity.class);
                startActivity(i);
                break;
            case R.id.closed_deals_FL:
                arguments.putInt(DealActivity.ARG_SECTION_ID, 2);
                i.putExtras(arguments);
                i.setClass(getActivity(), DealActivity.class);
                startActivity(i);
                break;
            case R.id.my_deals_FL:
                i = new Intent();
                i.setClass(getActivity(), ProjectsActivity.class);
                startActivity(i);
                break;
        }
    }
}
