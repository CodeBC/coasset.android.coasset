package co.nexlabs.android.coassets.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.fragment.ProjectListFragment;

/**
 * Created by SH on 30/May/2014.
 */
public class ProjectsActivity extends BaseActivity {
  String accType;
  String token;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_projects);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    ButterKnife.inject(this);

    accType = getIntent().getStringExtra(ProjectListFragment.TYPE);
    token = getIntent().getStringExtra(ProjectListFragment.TOKEN);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }


  @OnClick(R.id.participated_FL)
  void participatedProjects() {
    Log.e("", "Participated Project");
    Intent i = new Intent();
    Bundle arguments = new Bundle();
    arguments.putInt(ProjectListActivity.ARG_SECTION_ID, 1);
    arguments.putString(ProjectListFragment.TYPE, accType);
    arguments.putString(ProjectListFragment.TOKEN, token);
    i.putExtras(arguments);
    i.setClass(context, ProjectListActivity.class);
    startActivity(i);
  }

  @OnClick(R.id.owned_project_FL)
  void ownProjects() {
    Log.e("", "Own Project");
    Intent i = new Intent();
    Bundle arguments = new Bundle();
    arguments.putInt(ProjectListActivity.ARG_SECTION_ID, 2);
    arguments.putString(ProjectListFragment.TYPE, accType);
    arguments.putString(ProjectListFragment.TOKEN, token);
    i.putExtras(arguments);
    i.setClass(context, ProjectListActivity.class);
    startActivity(i);
  }
}
