package co.nexlabs.android.coassets.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.model.Token;

/**
 * Created by SH on 05/Jun/2014.
 */
public class TokenAdapter extends ArrayAdapter<Token> {
    private final Context mContext;
    private ArrayList<Token> tokens = new ArrayList<Token>();

    public TokenAdapter(Context context, ArrayList<Token> tokens) {
        super(context, R.layout.token_item, tokens);

        this.mContext = context;
        this.tokens = tokens;
        Log.e("", "Token Size (Adapter): " + tokens.size());
    }

    private ViewHolder getHolder(View v) {
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.token_item, null);
            holder = getHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.desc.setText(tokens.get(position).getDescription());
        holder.price.setText(tokens.get(position).getPrice() + " " + tokens.get(position).getCurrency());

        return vi;
    }

    static class ViewHolder {
        @InjectView(R.id.token_description)
        TextView desc;
        @InjectView(R.id.token_price)
        TextView price;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
