package co.nexlabs.android.coassets.model;

/**
 * Created by SH on 22/May/2014.
 */
public class UserItem {
    int id;
    String username;
    String first_name;
    String last_name;
    String email;
    String password;
    ProfileItem profile;
    AccountItem account;
    TokenItem tokens;

    public UserItem() {
        ProfileItem profile = new ProfileItem();
        setProfile(profile);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ProfileItem getProfile() {
        return profile;
    }

    public void setProfile(ProfileItem profile) {
        this.profile = profile;
    }

    public AccountItem getAccount() {
        return account;
    }

    public void setAccount(AccountItem account) {
        this.account = account;
    }

    public TokenItem getTokens() {
        return tokens;
    }

    public void setTokens(TokenItem tokens) {
        this.tokens = tokens;
    }
}
