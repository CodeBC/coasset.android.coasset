package co.nexlabs.android.coassets.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.adapter.NewsAdapter;
import co.nexlabs.android.coassets.model.NewsItem;

/**
 * Created by SH on 5/4/14
 */
public class NewsFragment extends ListFragment implements SearchView.OnQueryTextListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<NewsItem> mItems = new ArrayList<NewsItem>();
    private NewsAdapter mAdapter;
    private ProgressDialog dialog;
    private Callbacks mCallbacks;
    @InjectView(R.id.news_list_view)
    ListView listView;
    private SearchView mSearchView;
    Context context;

    public static NewsFragment newInstance(int sectionNumber) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public NewsFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallbacks = (Callbacks) activity;
        } catch (ClassCastException ex) {
            Log.e("", "Casting the activity as a Callbacks listener failed: " + ex);
            mCallbacks = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);
        dialog = new ProgressDialog(getActivity());
        context = getActivity().getApplicationContext();
        ButterKnife.inject(this, rootView);

        getAllNews();
        setHasOptionsMenu(true);

        return rootView;
    }

    private void getAllNews() {

        mItems = new ArrayList<NewsItem>();

        mAdapter = new NewsAdapter(getActivity().getApplicationContext(), mItems);
        listView.setAdapter(mAdapter);

        dialog.setMessage("Loading News");
        dialog.show();
        Ion.with(getActivity())
                .load("https://coassets.com/api/news/")
                .setLogging("iON", Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.e("", "Result: " + result);
                        dialog.dismiss();
                        if (result == null) {
                            Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show();
                        } else {
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = (JsonArray) parser.parse(result);
                            Gson gson = new GsonBuilder().create();

                            mItems.clear();
                            for (int i = 0; i < jArray.size(); i++) {
                                JsonObject obj = jArray.get(i).getAsJsonObject();
                                NewsItem item = gson.fromJson(obj, NewsItem.class);
                                mItems.add(item);
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (mCallbacks != null) {
            mCallbacks.onItemSelected(mItems.get(position));
        }
    }

    public interface Callbacks {
        public void onItemSelected(NewsItem news);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);
    }

    private void setupSearchView(MenuItem searchItem) {

        if (isAlwaysExpanded()) {
            mSearchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        dialog.setMessage("Loading News");
        dialog.show();

        Ion.with(getActivity())
                .load("https://coassets.com/api/news/filter/" + query)
                .setLogging("iON", Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.e("", "Result: " + result);
                        dialog.dismiss();
                        mItems.clear();
                        mAdapter.clear();
                        if (result == null) {
                            Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
                        } else {
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = (JsonArray) parser.parse(result);
                            Gson gson = new GsonBuilder().create();

                            for (int i = 0; i < jArray.size(); i++) {
                                JsonObject obj = jArray.get(i).getAsJsonObject();
                                NewsItem item = gson.fromJson(obj, NewsItem.class);
                                mItems.add(item);
                            }
                            mAdapter = new NewsAdapter(getActivity(), mItems);
                            listView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    protected boolean isAlwaysExpanded() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}
