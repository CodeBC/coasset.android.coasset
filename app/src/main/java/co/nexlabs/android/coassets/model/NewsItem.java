package co.nexlabs.android.coassets.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by SH on 5/2/14.
 */
public class NewsItem implements Serializable {
    public int id;
    public String title;
    public String author;
    public String date_added;
    public String section_name;
    public String text_content;

    public String getId() {
        return Integer.toString(id);
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat to = new SimpleDateFormat("dd/MM/yyyy");
        String dt = "";
        try {
            dt = to.format(from.parse(date_added));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }

    public String getContent() {
        return text_content;
    }
}
