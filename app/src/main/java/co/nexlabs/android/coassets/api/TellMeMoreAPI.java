package co.nexlabs.android.coassets.api;

import android.content.Context;

import co.nexlabs.android.coassets.utils.Constants;
import retrofit.RestAdapter;

/**
 * Created by SH on 23/May/2014.
 */
public class TellMeMoreAPI {
    private static TellMeMoreAPI mInstance;
    private Context mContext;
    private TellMeMoreService mService;

    public TellMeMoreAPI(Context context) {
        this.mContext = context;

        final RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(Constants.BASE_URL).build();
        mService = restAdapter.create(TellMeMoreService.class);
    }

    public static TellMeMoreAPI getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new TellMeMoreAPI(context);
        }
        return mInstance;
    }

    public TellMeMoreService getService() {
        return mService;
    }
}
