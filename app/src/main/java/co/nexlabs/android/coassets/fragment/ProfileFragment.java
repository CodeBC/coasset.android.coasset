package co.nexlabs.android.coassets.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.api.ProfileAPI;
import co.nexlabs.android.coassets.base.BaseFragment;
import co.nexlabs.android.coassets.model.UserItem;
import co.nexlabs.android.coassets.ui.ProjectsActivity;
import co.nexlabs.android.coassets.ui.TokenListActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by SH on 08/May/2014.
 */
public class ProfileFragment extends BaseFragment {
  private static final String ARG_SECTION_NUMBER = "section_number";
  @InjectView(R.id.user_name)
  EditText mUserName;
  @InjectView(R.id.country_prefix)
  EditText mCountryPrefix;
  @InjectView(R.id.address)
  EditText mAddress;
  @InjectView(R.id.city)
  EditText mCity;
  @InjectView(R.id.country)
  EditText mCountry;
  private boolean visible = false;
  @InjectView(R.id.personal_info_layout)
  LinearLayout personalLayout;
  @InjectView(R.id.password_layout)
  LinearLayout passwordLayout;
  @InjectView(R.id.username)
  TextView userName;
  @InjectView(R.id.account_type)
  TextView accType;
  @InjectView(R.id.token)
  TextView token;
  @InjectView(R.id.first_name)
  EditText firstName;
  @InjectView(R.id.last_name)
  EditText lastName;
  @InjectView(R.id.ph_num)
  EditText phNum;
  @InjectView(R.id.email)
  EditText email;
  @InjectView(R.id.new_password)
  EditText newPassword;
  @InjectView(R.id.confirm_password)
  EditText confirmPassword;
  @InjectView(R.id.logo)
  ImageView logo;
  ProgressDialog dialog;

  public static ProfileFragment newInstance(int sectionNumber) {
    ProfileFragment fragment = new ProfileFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    fragment.setArguments(args);
    return fragment;
  }

  public ProfileFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
    ButterKnife.inject(this, rootView);
    dialog = new ProgressDialog(getActivity());

    return rootView;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    getProfileInfo();
  }

  @OnClick(R.id.btn_personal_info)
  public void showProfileInfo() {
    if (!visible) {
      personalLayout.setVisibility(View.VISIBLE);
      firstName.requestFocus();
      visible = true;
    } else {
      personalLayout.setVisibility(View.GONE);
      visible = false;
    }
  }

  @OnClick(R.id.btn_personal_info_submit)
  public void updatePersonalInfo() {
    dialog.setMessage("Loading");
    dialog.show();
    if (!TextUtils.isEmpty(firstName.getText()) && !lastName.getText().toString().equalsIgnoreCase("") && !email.getText().toString().equalsIgnoreCase("") && !phNum.getText().toString().equalsIgnoreCase("")) {
      String uName = mUserName.getText().toString();
      String fName = firstName.getText().toString();
      String lName = lastName.getText().toString();
      String mail = email.getText().toString();
      String cPrefix = mCountryPrefix.getText().toString();
      String ph = phNum.getText().toString();
      String add = mAddress.getText().toString();
      String city = mCity.getText().toString();
      String count = mCountry.getText().toString();

      ProfileAPI.getInstance(context).getService().updateProfile("Bearer " + pref.getAccessToken(), uName, mail, lName, fName, cPrefix, ph, add, city, count, new Callback<UserItem>() {
        @Override
        public void success(UserItem user, Response response) {
          Log.e("", "Response: " + response);
          dialog.dismiss();
          if (response.getStatus() == 201) {
            Toast.makeText(context, "Successfully Updated.", Toast.LENGTH_SHORT).show();
          } else {
            Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show();
          }
        }

        @Override
        public void failure(RetrofitError error) {
          dialog.dismiss();
          Log.e("", "Response: " + error.getLocalizedMessage());
          Toast.makeText(context, "Something went wrong. Try again.", Toast.LENGTH_SHORT).show();
        }
      });
    } else {
      Toast.makeText(context, "Please fill all the form.", Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.btn_change_password)
  public void showChangePassword() {
    if (!visible) {
      passwordLayout.setVisibility(View.VISIBLE);
      firstName.requestFocus();
      visible = true;
    } else {
      passwordLayout.setVisibility(View.GONE);
      visible = false;
    }
  }

  @OnClick(R.id.btn_password_submit)
  public void changePassword() {
    String newPswd = newPassword.getText().toString();
    String conPswd = confirmPassword.getText().toString();

    if (!newPswd.equalsIgnoreCase("") && !conPswd.equalsIgnoreCase("")) {
      if (newPswd.equalsIgnoreCase(conPswd)) {
        dialog.setMessage("Please wait...");
        dialog.show();
        ProfileAPI.getInstance(context).getService().changePassword("Bearer " + pref.getAccessToken(), newPswd, new Callback<String>() {
          @Override
          public void success(String s, Response response) {
            dialog.dismiss();
            Log.e("", "Response: " + response.getStatus());
            Toast.makeText(context, "Password Changed Successfully.", Toast.LENGTH_SHORT).show();
            passwordLayout.setVisibility(View.GONE);
          }

          @Override
          public void failure(RetrofitError error) {
            dialog.dismiss();
            Log.e("", "Response: " + error.getLocalizedMessage());
            Toast.makeText(context, "Something went wrong. Try again.", Toast.LENGTH_SHORT).show();
          }
        });
      } else {
        Toast.makeText(context, "Password didn't match", Toast.LENGTH_SHORT).show();
      }
    } else {
      Toast.makeText(context, "Please fill all the form.", Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.btn_projects)
  public void openProjects() {
    Intent i = new Intent();
    i.putExtra(ProjectListFragment.TYPE, accType.getText().toString());
    i.putExtra(ProjectListFragment.TOKEN, token.getText().toString());
    i.setClass(getActivity(), ProjectsActivity.class);
    startActivity(i);
  }

  public void getProfileInfo() {
    dialog.setMessage("Loading Profile Info");
    dialog.show();

    ProfileAPI.getInstance(context).getService().getUserInfo("Bearer " + pref.getAccessToken(), new Callback<UserItem>() {
      @Override
      public void success(UserItem s, Response response) {
        dialog.dismiss();
        mUserName.setText(s.getUsername());
        userName.setText(s.getUsername());
        if (s.getAccount().getAccount_type() == null) {
          accType.setText("Account Type : Not Available");
        } else {
          accType.setText("Account Type : " + s.getAccount().getAccount_type());
        }
        pref.setAccType(accType.getText().toString());
        token.setText("Token : " + s.getTokens().getCredit_qty());
        pref.setToken(token.getText().toString());
        firstName.setText(s.getFirst_name());
        lastName.setText(s.getLast_name());
        email.setText(s.getEmail());
        mCountryPrefix.setText(s.getProfile().getCountry_prefix());
        phNum.setText(s.getProfile().getCell_phone() + "");
        mAddress.setText(s.getProfile().getAddress_1());
        mCity.setText(s.getProfile().getCity());
        mCountry.setText(s.getProfile().getCountry());
        pref.setExpiry(s.getAccount().getAccount_expiry());
      }

      @Override
      public void failure(RetrofitError error) {
        dialog.dismiss();
        Log.e("", "Error: " + error.getLocalizedMessage());
      }
    });
  }

  @OnClick(R.id.btn_buy_token)
  void buyToken() {
    Intent i = new Intent();
    i.setClass(context, TokenListActivity.class);
    startActivity(i);
  }
}
