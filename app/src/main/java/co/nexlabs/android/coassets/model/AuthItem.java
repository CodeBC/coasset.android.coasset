package co.nexlabs.android.coassets.model;

/**
 * Created by SH on 5/2/14.
 */
public class AuthItem {
    public String access_token;
    public String token_type;
    public String refresh_token;
    public String scope;
}
