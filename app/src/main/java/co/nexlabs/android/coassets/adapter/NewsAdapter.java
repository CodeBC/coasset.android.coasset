package co.nexlabs.android.coassets.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.model.NewsItem;

/**
 * Created by SH on 5/2/14.
 */
public class NewsAdapter extends ArrayAdapter<NewsItem> {
    private final Context mContext;
    private ArrayList<NewsItem> news = new ArrayList<NewsItem>();

    public NewsAdapter(Context context, ArrayList<NewsItem> news) {
        super(context, R.layout.news_item, news);

        this.mContext = context;
        this.news = news;
        Log.e("", "News Size (Adapter): " + news.size());
    }

    private ViewHolder getHolder(View v) {
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.news_item, null);
            holder = getHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.newsTitle.setText(news.get(position).getTitle());
        holder.newsDate.setText(news.get(position).getDate());
        holder.newsAuthor.setText(news.get(position).getAuthor());

        return vi;
    }

    static class ViewHolder {
        @InjectView(R.id.txt_news_title)
        TextView newsTitle;
        @InjectView(R.id.txt_date)
        TextView newsDate;
        @InjectView(R.id.txt_news_author)
        TextView newsAuthor;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
