package co.nexlabs.android.coassets.model;

/**
 * Created by SH on 22/May/2014.
 */
public class AccountItem {
    String account_type;
    String account_expiry;

    public AccountItem() {
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getAccount_expiry() {
        return account_expiry;
    }

    public void setAccount_expiry(String account_expiry) {
        this.account_expiry = account_expiry;
    }
}
