package co.nexlabs.android.coassets.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.nexlabs.android.coassets.R;
import co.nexlabs.android.coassets.base.BaseActivity;
import co.nexlabs.android.coassets.model.AuthItem;

public class LoginActivity extends BaseActivity {

  private final String clientId = "6c85f05b29e609bcacfd";
  private final String clientSecret = "207219bcc24a06f112011fc3574f389b71339121";

  // UI references.
  @InjectView(R.id.email)
  public AutoCompleteTextView mEmailView;
  @InjectView(R.id.password)
  public EditText mPasswordView;
  @InjectView(R.id.login_progress)
  public View mProgressView;
  @InjectView(R.id.login_form)
  public View mLoginFormView;
  @InjectView(R.id.txt_new_acc)
  public TextView btnNew;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (pref.isExist(pref.mAccessToken)) {
      Intent i = new Intent();
      i.setClass(context, MainActivity.class);
      startActivity(i);
      finish();
    } else {
      setContentView(R.layout.activity_login);
      ButterKnife.inject(this);

      mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
          if (id == R.id.login || id == EditorInfo.IME_NULL) {
            attemptLogin();
            return true;
          }
          return false;
        }
      });
    }
  }

  @OnClick(R.id.email_sign_in_button)
  public void attemptLogin() {

    // Reset errors.
    mEmailView.setError(null);
    mPasswordView.setError(null);

    // Store values at the time of the login attempt.
    String email = mEmailView.getText().toString();
    String password = mPasswordView.getText().toString();

    boolean cancel = false;
    View focusView = null;

    // Check for a valid email address.
    if (TextUtils.isEmpty(email)) {
      mEmailView.setError(getString(R.string.error_field_required));
      focusView = mEmailView;
      cancel = true;
    }

    if (cancel) {
      // There was an error; don't attempt login and focus the first
      // form field with an error.
      focusView.requestFocus();
    } else {
      // Show a progress spinner, and kick off a background task to
      // perform the user login attempt.
      showProgress(true);
      login(email, password);
    }
  }

  /**
   * Shows the progress UI and hides the login form.
   */
  @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
  public void showProgress(final boolean show) {
    // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
    // for very easy animations. If available, use these APIs to fade-in
    // the progress spinner.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
      int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

      mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
      mLoginFormView.animate().setDuration(shortAnimTime).alpha(
          show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
      });

      mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
      mProgressView.animate().setDuration(shortAnimTime).alpha(
          show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
      });
    } else {
      // The ViewPropertyAnimator APIs are not available, so simply show
      // and hide the relevant UI components.
      mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
      mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }
  }

  @OnClick(R.id.txt_new_acc)
  public void newAccount() {
    Intent i = new Intent();
    i.setClass(this, RegisterActivity.class);
    startActivity(i);
  }

  public void login(String username, String pswd) {

    //"client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&grant_type=password&username=YOUR_USERNAME&password=YOUR_PASSWORD"
    Ion.with(context)
        .load("https://coassets.com/oauth2/access_token/")
        .setLogging("iON", Log.DEBUG)
        .setBodyParameter("client_id", clientId)
        .setBodyParameter("client_secret", clientSecret)
        .setBodyParameter("grant_type", "password")
        .setBodyParameter("username", username)
        .setBodyParameter("password", pswd)
        .asString()
        .setCallback(new FutureCallback<String>() {
          @Override
          public void onCompleted(Exception e, String result) {

            if (result.equalsIgnoreCase("{\"error\": \"invalid_grant\"}")) {
              showProgress(false);
              Toast.makeText(context, "User Name or Password is incorrect.", Toast.LENGTH_SHORT).show();
            } else {
              JsonParser parser = new JsonParser();

              JsonObject jobj_main = (JsonObject) parser.parse(result);

              Gson gson = new GsonBuilder().create();

              AuthItem auth = gson.fromJson(jobj_main, AuthItem.class);

              pref.setAccessToken(auth.access_token);

              Intent i = new Intent();
              i.setClass(context, MainActivity.class);
              startActivity(i);
              finish();
            }
          }
        });
  }
}



