package co.nexlabs.android.coassets.api;

import com.google.gson.JsonObject;

import co.nexlabs.android.coassets.utils.Constants;
import retrofit.Callback;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by SH on 23/May/2014.
 */
public interface TellMeMoreService {
    @POST(Constants.TELLME_MORE + "{id}/")
    void tellMeMore(@Header("Authorization") String authorization, @Path("id") int id, Callback<JsonObject> callback);

    @POST(Constants.SUBSCRIBE_API + "{id}/")
    void subscribe(@Header("Authorization") String authorization, @Path("id") int id, Callback<JsonObject> callback);
}
